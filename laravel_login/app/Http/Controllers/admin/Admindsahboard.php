<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Illuminate\Http\Request;

class Admindsahboard extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->role != 1 && Auth::user()->role != 2) {
            return redirect()->to('/');
        }
        $datas = User::get();
        return view('admin.index', compact('datas'));
    }
}
